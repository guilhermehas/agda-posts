---
title : Verified Programming Based on Univalent Foundations in Agda - Part 0
---

## Simple Types, Intro to Equality, and Proofs by Case Analysis

### Introduction

These notes are a product of cabin fever and are primarily based on three resources:

* [Introduction to Univalent Foundations of Mathematics with Agda](https://www.cs.bham.ac.uk/~mhe/HoTT-UF-in-Agda-Lecture-Notes/HoTT-UF-Agda.html) by Martín Hötzel Escardó
* [Software Foundations](https://softwarefoundations.cis.upenn.edu/current/index.html) by Pierce et al. 
* [Verified Functional Programming in Agda](https://dl.acm.org/doi/book/10.1145/2841316) by Aaron Stump

The latter two provide an introduction to using dependently-typed programming
languages (Coq, Agda, Idris, F-Star, etc.) to not only write programs but also to
formally verify the programs we have written.
The first resource has an entirely different purpose.
Univalent Foundations
(I'm making a very personal choice in saying Univalent Foundations instead of
Homotopy Type Theory) is a project to use a Martin-Löf Type Theory
(MLTT) plus a few extra goodies as the substrate in which we can embed
mathematics as an alternative to set theory or category theory. 
In a classical setting, we could ideally unwind definitions of a mathematical
object and see there's nothing but sets all the way down.
While in MLTT, a mathematical object would be a term of a specific type.
One of the benefits of using MLTT is that not only can a type represent a class
of objects but can also represent a mathematical statement/proposition and a term of that that
type is a proof of that statement.
Compare that to a proposition in set theory which is not a set but rather a sentence of the
first-order logic.
Plus, verifying our proof is correct is equivalent to just asking a compiler to verify that
our proof/program type check.
I know the "just" in that previous sentence trivializes the task of correctly implementing
a programming language like Agda, but I hope anyone that would be offended can
chalk it up to enthusiasm.
Escardó and the
[HoTT Book](https://homotopytypetheory.org/book/)
do a much better job providing exposition than I could ever hope to do.

These notes are more for me than anyone else and were born from a silly question:
"What happens when we apply Univalent Foundations to program verification?"
If UF/HoTT is useful for Monoids and Algebraic Rings, then what happens when we use it
to reason about lists and lexers?
Writing and posting them lets me check a few different items off my list of things I wanted to
learn.
But I hope someone somewhere can learn something from them, and if not, then I hope
I at least get some (polite) corrections and a better understanding.

Quick note: there's even more exposition before actual programming begins.
We're not trying to build our house on sand.
Feel free to scroll through.

We start by specfiying the pragma with which we will be working.
<pre class="Agda">
<a id="2944" class="Symbol">{-#</a> <a id="2948" class="Keyword">OPTIONS</a> <a id="2956" class="Pragma">--without-K</a> <a id="2968" class="Pragma">--exact-split</a> <a id="2982" class="Pragma">--safe</a> <a id="2989" class="Symbol">#-}</a>

</pre>
The flag `--without-K` disables Streicher's
[Axiom K](https://ncatlab.org/nlab/show/axiom+K+%28type+theory%29).
This axiom is inconsistent with UF.
We will explain exactly what it is and how it is inconsistent later.
Disabling Axiom K has a practical impact on writing Agda code.
Case-splitting becomes weaker.

`--exact-split` disallows definitions which include pattern
matching clauses which do not hold definitionally.
The
[Agda documentation](https://agda.readthedocs.io/en/v2.6.1/language/function-definitions.html#case-trees)
does a good job explaining the consequences of enabling this flag. 

`--safe` disables features known to make Agda inconsistent.
One of the features `--safe` disables are
[rewrite rules](https://agda.readthedocs.io/en/v2.6.1/language/rewriting.html).
We will soon develop a notion of equality and `--safe` prevents us from using
the rewrite construction with our equality of interest. 

Notice, all the flags make our lives more difficult and will make some of
our proofs look very different than the ones in Stump or Pierce et al.
So I don't think asking if UF is practical for program verification is the
dumbest question ever. 

<pre class="Agda">
<a id="4169" class="Keyword">module</a> <a id="4176" href="Basics.html" class="Module">Basics</a> <a id="4183" class="Keyword">where</a>

<a id="4190" class="Keyword">open</a> <a id="4195" class="Keyword">import</a> <a id="4202" href="Agda.Primitive.html" class="Module">Agda.Primitive</a> <a id="4217" class="Keyword">public</a>

</pre>We declare our module and import the `Agda.Primitive` module.
Importing `Agda.Primitive` allows us to explicitly refer to
[Universe levels](https://agda.readthedocs.io/en/v2.6.1/language/built-ins.html#universe-levels).

This interlude is not Agda specific, but will explain what are Universe levels and
why we need to use them explicitly.
Everything should have type: `0` is of type `ℕ`, `"hello world"` is of type `String` and 
`ℕ` and `String` are of type `Type`.
The natural question is what is the type of `Type`?
Naively we would think `Type` is of type `Type`, but anyone who has heard of
[Russell's Paradox](https://en.wikipedia.org/wiki/Russell%27s_paradox)
should feel a little uneasy coming to that conclusion.
and that unease is justified.
Assuming `Type : Type` leads to inconsistencies.
The flag `--safe` actually prevents the `--type-in-type` flag which allows a user to encode
such an inonsistency.

The solution is to come up with a hierarchy of type universes
`Type₀`, `Type₁`, `Type₂`, ... such that
`Type₀ : Type₁`, `Type₁ : Type₂`, `Type₂ : Type₃`, ...
Agda uses write `Set` instead of `Type`, so `ℕ : Set₀`, `String : Set₀` while
`Set₀ : Set₁`, `Set₁ : Set₂` and so on.
The subscript is called the universe level.
There are the following operations on universe levels: a base level `lzero`,
the successor `lsuc` and the least upper bound `_⊔_` which act as you would expect.
These are the only valid universe level operators.
If you try to define a plus operator `lplus : Level → Level → Level`
analagous to addition on ℕ, Agda won't let you go far.
Agda won't even let you case-split on the arguments. 
Warning: numerals are used denote the universe levels for convenience
but universe levels are not the type `ℕ`.
`Set₀` is just short hand for `Set lzero`, while `Set₁` `Set₂` are defined
as `Set (lsuc lzero)` and `Set (lsuc (lsuc (lzero)))`.
An expression like `λ (n : Level) → Set n` is malformed because we can't find
a universe in which this expression should reside.
There is a *kind* `Setω` which can contain such an expression but
that's outside the scope of our conversation and our lives are
difficult enough already. 

This is a lot of front-loaded theory.
Let's actually begin to write some code. 


<pre class="Agda">
<a id="6470" class="Keyword">variable</a>
  <a id="6481" href="Basics.html#6481" class="Generalizable">ℓ</a> <a id="6483" href="Basics.html#6483" class="Generalizable">ℓ&#39;</a> <a id="6486" href="Basics.html#6486" class="Generalizable">ℓ&#39;&#39;</a> <a id="6490" class="Symbol">:</a> <a id="6492" href="Agda.Primitive.html#408" class="Postulate">Level</a>

</pre>This declaration allows us to refer to universe levels in
function definitions without having to pass them as arguments, which
saves us on some busy work.
We can write `foo : (X : Set ℓ) → X → X` instead of
`foo : (ℓ : Level) (X : Set ℓ) → X → X`.
The `variable` block instructs Agda to fill in the missing bindings for us. 

<pre class="Agda">
<a id="6834" class="Keyword">data</a> <a id="𝟙"></a><a id="6839" href="Basics.html#6839" class="Datatype">𝟙</a> <a id="6841" class="Symbol">:</a> <a id="6843" class="PrimitiveType">Set</a> <a id="6847" class="Keyword">where</a>
  <a id="𝟙.*"></a><a id="6855" href="Basics.html#6855" class="InductiveConstructor">*</a> <a id="6857" class="Symbol">:</a> <a id="6859" href="Basics.html#6839" class="Datatype">𝟙</a>

</pre>This is admittedly a lot of work to introduce such a trivial type.
𝟙 is the unit type and equivalent to `()` in Haskell and Rust,
`unit` in OCAML or F#, and `True` in Coq.
As suggested by the Coq equivalent, `𝟙` represents
propositional truth in the types-as-propositions interpretation of
type theory.
Note: `Set` written without any subscript or universe level
argument is just `Set₀`.

Readers familiar with Coq, or who have read something like
[The Little Typer](https://mitpress.mit.edu/books/little-typer) know
that an inductive data-type has a corresponding inductive principle.
These inductive principles are what allows us to
construct functions on or prove properties about that type.
That last statement is a bit redundant since constructing
functions/terms *is* how a proofs are constructed.
Usually we just use pattern matching to make definitions but
sometimes it is actually useful, either for practical or
pedagogical reasons, to resort to inductive principles directly.
For our trivial type `𝟙`, the corresponding inductive
principle is

<pre class="Agda">
<a id="𝟙-induction"></a><a id="7926" href="Basics.html#7926" class="Function">𝟙-induction</a> <a id="7938" class="Symbol">:</a> <a id="7940" class="Symbol">(</a><a id="7941" href="Basics.html#7941" class="Bound">A</a> <a id="7943" class="Symbol">:</a> <a id="7945" href="Basics.html#6839" class="Datatype">𝟙</a> <a id="7947" class="Symbol">→</a> <a id="7949" class="PrimitiveType">Set</a> <a id="7953" href="Basics.html#6481" class="Generalizable">ℓ</a><a id="7954" class="Symbol">)</a> <a id="7956" class="Symbol">→</a> <a id="7958" href="Basics.html#7941" class="Bound">A</a> <a id="7960" href="Basics.html#6855" class="InductiveConstructor">*</a>
            <a id="7974" class="Symbol">→</a> <a id="7976" class="Symbol">(</a><a id="7977" href="Basics.html#7977" class="Bound">x</a> <a id="7979" class="Symbol">:</a> <a id="7981" href="Basics.html#6839" class="Datatype">𝟙</a><a id="7982" class="Symbol">)</a> <a id="7984" class="Symbol">→</a> <a id="7986" href="Basics.html#7941" class="Bound">A</a> <a id="7988" href="Basics.html#7977" class="Bound">x</a>
<a id="7990" href="Basics.html#7926" class="Function">𝟙-induction</a> <a id="8002" href="Basics.html#8002" class="Bound">A</a> <a id="8004" href="Basics.html#8004" class="Bound">a</a> <a id="8006" href="Basics.html#6855" class="InductiveConstructor">*</a> <a id="8008" class="Symbol">=</a> <a id="8010" href="Basics.html#8004" class="Bound">a</a>

</pre>To translate the above into a more informal language:
If we have some property `A` and proof that `A` holds
for the value `*`, then we we know `A` holds for all
elements of type `𝟙`, which makes sense since there is exactly
one-term (`*`) of type `𝟙`.
It's a pretty boring inductive principle,
but 𝟙 is not the most interesting type.

The non-dependent analog is the recursion principle for `𝟙`, and
is how we construct non-dependent functions from `𝟙` to a type
`B`.

<pre class="Agda"><a id="𝟙-recursion"></a><a id="8490" href="Basics.html#8490" class="Function">𝟙-recursion</a> <a id="8502" class="Symbol">:</a> <a id="8504" class="Symbol">(</a><a id="8505" href="Basics.html#8505" class="Bound">B</a> <a id="8507" class="Symbol">:</a> <a id="8509" class="PrimitiveType">Set</a> <a id="8513" href="Basics.html#6481" class="Generalizable">ℓ</a><a id="8514" class="Symbol">)</a> <a id="8516" class="Symbol">→</a> <a id="8518" href="Basics.html#8505" class="Bound">B</a>
            <a id="8532" class="Symbol">→</a> <a id="8534" href="Basics.html#6839" class="Datatype">𝟙</a> <a id="8536" class="Symbol">→</a> <a id="8538" href="Basics.html#8505" class="Bound">B</a>
<a id="8540" href="Basics.html#8490" class="Function">𝟙-recursion</a> <a id="8552" href="Basics.html#8552" class="Bound">B</a> <a id="8554" class="Symbol">=</a> <a id="8556" href="Basics.html#7926" class="Function">𝟙-induction</a> <a id="8568" class="Symbol">(λ</a> <a id="8571" href="Basics.html#8571" class="Bound">_</a> <a id="8573" class="Symbol">→</a> <a id="8575" href="Basics.html#8552" class="Bound">B</a><a id="8576" class="Symbol">)</a>

</pre>
One interesting property of `𝟙` is that it is a terminal object.
For any type `A`, there is a unique function from `A` to `𝟙`,
which we can and will prove later.

<pre class="Agda">
<a id="!𝟙"></a><a id="8752" href="Basics.html#8752" class="Function">!𝟙</a> <a id="8755" class="Symbol">:</a> <a id="8757" class="Symbol">{</a><a id="8758" href="Basics.html#8758" class="Bound">C</a> <a id="8760" class="Symbol">:</a> <a id="8762" class="PrimitiveType">Set</a> <a id="8766" href="Basics.html#6481" class="Generalizable">ℓ</a><a id="8767" class="Symbol">}</a> <a id="8769" class="Symbol">→</a> <a id="8771" href="Basics.html#8758" class="Bound">C</a> <a id="8773" class="Symbol">→</a> <a id="8775" href="Basics.html#6839" class="Datatype">𝟙</a>
<a id="8777" href="Basics.html#8752" class="Function">!𝟙</a> <a id="8780" href="Basics.html#8780" class="Bound">c</a> <a id="8782" class="Symbol">=</a> <a id="8784" href="Basics.html#6855" class="InductiveConstructor">*</a>

</pre>
We also define an explicit version.

<pre class="Agda">
<a id="!𝟙&#39;"></a><a id="8834" href="Basics.html#8834" class="Function">!𝟙&#39;</a> <a id="8838" class="Symbol">:</a> <a id="8840" class="Symbol">(</a><a id="8841" href="Basics.html#8841" class="Bound">C</a> <a id="8843" class="Symbol">:</a> <a id="8845" class="PrimitiveType">Set</a> <a id="8849" href="Basics.html#6481" class="Generalizable">ℓ</a><a id="8850" class="Symbol">)</a> <a id="8852" class="Symbol">→</a> <a id="8854" href="Basics.html#8841" class="Bound">C</a> <a id="8856" class="Symbol">→</a> <a id="8858" href="Basics.html#6839" class="Datatype">𝟙</a>
<a id="8860" href="Basics.html#8834" class="Function">!𝟙&#39;</a> <a id="8864" href="Basics.html#8864" class="Bound">C</a> <a id="8866" class="Symbol">=</a> <a id="8868" href="Basics.html#8752" class="Function">!𝟙</a> 

</pre>
Then there's the other trivial type...

<pre class="Agda">
<a id="8923" class="Keyword">data</a> <a id="𝟘"></a><a id="8928" href="Basics.html#8928" class="Datatype">𝟘</a> <a id="8930" class="Symbol">:</a> <a id="8932" class="PrimitiveType">Set</a> <a id="8936" class="Keyword">where</a>

</pre>
...the empty type 𝟘, which contains zero terms.
Idris's equivalent is `Void`,
and the Coq analog is `False`.
As Coq's equivalent suggests, `𝟘` will represent propositional
falsehood.
Since `𝟘` has no constructors, we should not be able to
construct/prove such a falsity.
Consequently, its induction principle is simpler yet
stranger than `𝟙`.


<pre class="Agda">
<a id="𝟘-induction"></a><a id="9298" href="Basics.html#9298" class="Function">𝟘-induction</a> <a id="9310" class="Symbol">:</a> <a id="9312" class="Symbol">(</a><a id="9313" href="Basics.html#9313" class="Bound">A</a> <a id="9315" class="Symbol">:</a> <a id="9317" href="Basics.html#8928" class="Datatype">𝟘</a> <a id="9319" class="Symbol">→</a> <a id="9321" class="PrimitiveType">Set</a> <a id="9325" href="Basics.html#6481" class="Generalizable">ℓ</a><a id="9326" class="Symbol">)</a>
            <a id="9340" class="Symbol">→</a> <a id="9342" class="Symbol">(</a><a id="9343" href="Basics.html#9343" class="Bound">x</a> <a id="9345" class="Symbol">:</a> <a id="9347" href="Basics.html#8928" class="Datatype">𝟘</a><a id="9348" class="Symbol">)</a> <a id="9350" class="Symbol">→</a> <a id="9352" href="Basics.html#9313" class="Bound">A</a> <a id="9354" href="Basics.html#9343" class="Bound">x</a>
<a id="9356" href="Basics.html#9298" class="Function">𝟘-induction</a> <a id="9368" href="Basics.html#9368" class="Bound">A</a> <a id="9370" class="Symbol">()</a>

</pre>What this says is that any property follows vacuously from a false assumption.
The principle in classical logic is called "ex falso quodlibet" --
from falsehood, anything follows.
The Coq tactic which corresponds to this induction principle is
appropriately called `exfalso`.
The line containing `()` is Agda's special syntax for declaring an
absurd pattern.
If we tried to write the definition `𝟘-induction A x = ?` and
asked Agda to case-split on `x` then Agda will replace the definition
with the line `𝟘-induction A ()` above.
Which happens because there are no constructors `𝟘`.
We will try to avoid using `()`-like arguments elsewhere;
they do not strictly belong to the realm of UF/HoTT arguments.


We also have the non-dependent recursion principle.

<pre class="Agda">
<a id="𝟘-recursion"></a><a id="10143" href="Basics.html#10143" class="Function">𝟘-recursion</a> <a id="10155" class="Symbol">:</a> <a id="10157" class="Symbol">(</a><a id="10158" href="Basics.html#10158" class="Bound">B</a> <a id="10160" class="Symbol">:</a> <a id="10162" class="PrimitiveType">Set</a> <a id="10166" href="Basics.html#6481" class="Generalizable">ℓ</a><a id="10167" class="Symbol">)</a>
            <a id="10181" class="Symbol">→</a> <a id="10183" href="Basics.html#8928" class="Datatype">𝟘</a> <a id="10185" class="Symbol">→</a> <a id="10187" href="Basics.html#10158" class="Bound">B</a>
<a id="10189" href="Basics.html#10143" class="Function">𝟘-recursion</a> <a id="10201" href="Basics.html#10201" class="Bound">B</a> <a id="10203" class="Symbol">=</a> <a id="10205" href="Basics.html#9298" class="Function">𝟘-induction</a> <a id="10217" class="Symbol">(λ</a> <a id="10220" href="Basics.html#10220" class="Bound">_</a> <a id="10222" class="Symbol">→</a> <a id="10224" href="Basics.html#10201" class="Bound">B</a><a id="10225" class="Symbol">)</a>

</pre>
In contrast to `𝟙`, `𝟘` is an initial object: for any type, there
is a unique function from `𝟘`.

<pre class="Agda">
<a id="!𝟘"></a><a id="10336" href="Basics.html#10336" class="Function">!𝟘</a> <a id="10339" class="Symbol">:</a> <a id="10341" class="Symbol">{</a><a id="10342" href="Basics.html#10342" class="Bound">C</a> <a id="10344" class="Symbol">:</a> <a id="10346" class="PrimitiveType">Set</a> <a id="10350" href="Basics.html#6481" class="Generalizable">ℓ</a><a id="10351" class="Symbol">}</a> <a id="10353" class="Symbol">→</a> <a id="10355" href="Basics.html#8928" class="Datatype">𝟘</a> <a id="10357" class="Symbol">→</a> <a id="10359" href="Basics.html#10342" class="Bound">C</a>
<a id="10361" href="Basics.html#10336" class="Function">!𝟘</a> <a id="10364" class="Symbol">{</a><a id="10365" href="Basics.html#10365" class="Bound">ℓ</a><a id="10366" class="Symbol">}</a> <a id="10368" class="Symbol">{</a><a id="10369" href="Basics.html#10369" class="Bound">C</a><a id="10370" class="Symbol">}</a> <a id="10372" class="Symbol">=</a> <a id="10374" href="Basics.html#10143" class="Function">𝟘-recursion</a> <a id="10386" href="Basics.html#10369" class="Bound">C</a>

<a id="!𝟘&#39;"></a><a id="10389" href="Basics.html#10389" class="Function">!𝟘&#39;</a> <a id="10393" class="Symbol">:</a> <a id="10395" class="Symbol">(</a><a id="10396" href="Basics.html#10396" class="Bound">C</a> <a id="10398" class="Symbol">:</a> <a id="10400" class="PrimitiveType">Set</a> <a id="10404" href="Basics.html#6481" class="Generalizable">ℓ</a><a id="10405" class="Symbol">)</a> <a id="10407" class="Symbol">→</a> <a id="10409" href="Basics.html#8928" class="Datatype">𝟘</a> <a id="10411" class="Symbol">→</a> <a id="10413" href="Basics.html#10396" class="Bound">C</a>
<a id="10415" href="Basics.html#10389" class="Function">!𝟘&#39;</a> <a id="10419" href="Basics.html#10419" class="Bound">C</a> <a id="10421" class="Symbol">=</a> <a id="10423" href="Basics.html#10336" class="Function">!𝟘</a>

</pre>
`𝟘` is not the only empty type but it will be our canonical empty type.
We define a sort-of predicate `is-empty X` on types whose elements will be
proof that the type `X` is empty.
In the types-as-propositions interpretation, this `is-empty` is the
negation of a proposition. 

<pre class="Agda">
<a id="is-empty"></a><a id="10715" href="Basics.html#10715" class="Function">is-empty</a> <a id="10724" class="Symbol">:</a> <a id="10726" class="PrimitiveType">Set</a> <a id="10730" href="Basics.html#6481" class="Generalizable">ℓ</a> <a id="10732" class="Symbol">→</a> <a id="10734" class="PrimitiveType">Set</a> <a id="10738" href="Basics.html#6481" class="Generalizable">ℓ</a>
<a id="10740" href="Basics.html#10715" class="Function">is-empty</a> <a id="10749" href="Basics.html#10749" class="Bound">X</a> <a id="10751" class="Symbol">=</a> <a id="10753" href="Basics.html#10749" class="Bound">X</a> <a id="10755" class="Symbol">→</a> <a id="10757" href="Basics.html#8928" class="Datatype">𝟘</a>

<a id="¬"></a><a id="10760" href="Basics.html#10760" class="Function">¬</a> <a id="10762" class="Symbol">:</a> <a id="10764" class="PrimitiveType">Set</a> <a id="10768" href="Basics.html#6481" class="Generalizable">ℓ</a> <a id="10770" class="Symbol">→</a> <a id="10772" class="PrimitiveType">Set</a> <a id="10776" href="Basics.html#6481" class="Generalizable">ℓ</a>
<a id="10778" href="Basics.html#10760" class="Function">¬</a> <a id="10780" href="Basics.html#10780" class="Bound">X</a> <a id="10782" class="Symbol">=</a> <a id="10784" href="Basics.html#10780" class="Bound">X</a> <a id="10786" class="Symbol">→</a> <a id="10788" href="Basics.html#8928" class="Datatype">𝟘</a>

</pre>
Let's turn to something less trivial yet still simple and
define a Boolean type.

<pre class="Agda">
<a id="10883" class="Keyword">data</a> <a id="𝔹"></a><a id="10888" href="Basics.html#10888" class="Datatype">𝔹</a> <a id="10890" class="Symbol">:</a> <a id="10892" class="PrimitiveType">Set</a> <a id="10896" class="Keyword">where</a>
  <a id="𝔹.tt"></a><a id="10904" href="Basics.html#10904" class="InductiveConstructor">tt</a> <a id="𝔹.ff"></a><a id="10907" href="Basics.html#10907" class="InductiveConstructor">ff</a> <a id="10910" class="Symbol">:</a> <a id="10912" href="Basics.html#10888" class="Datatype">𝔹</a>

</pre>  We tell the compiler that 𝔹 does indeed act like
  booleans.
  This isn't strictly needed but doesn't hurt.
<pre class="Agda">

<a id="11035" class="Symbol">{-#</a> <a id="11039" class="Keyword">BUILTIN</a> <a id="11047" class="Pragma">BOOL</a> <a id="11052" href="Basics.html#10888" class="Datatype">𝔹</a> <a id="11054" class="Symbol">#-}</a>
<a id="11058" class="Symbol">{-#</a> <a id="11062" class="Keyword">BUILTIN</a> <a id="11070" class="Pragma">TRUE</a> <a id="11075" href="Basics.html#10904" class="InductiveConstructor">tt</a> <a id="11078" class="Symbol">#-}</a>
<a id="11082" class="Symbol">{-#</a> <a id="11086" class="Keyword">BUILTIN</a> <a id="11094" class="Pragma">FALSE</a> <a id="11100" href="Basics.html#10907" class="InductiveConstructor">ff</a> <a id="11103" class="Symbol">#-}</a>

</pre>  We then define the usual and common operators on Booleans
  and the set precedence an associativity so we are able to write
  Boolean expressions more concisely.
  We use the `_` pattern to emphasize when a function doesn't
  depend on the value of an argument. 

<pre class="Agda">
<a id="11383" class="Keyword">infix</a> <a id="11389" class="Number">7</a> <a id="11391" href="Basics.html#11463" class="Function Operator">~_</a>
<a id="11394" class="Keyword">infixl</a> <a id="11401" class="Number">6</a> <a id="11403" href="Basics.html#11729" class="Function Operator">_xor_</a> <a id="11409" href="Basics.html#11827" class="Function Operator">_nand_</a>
<a id="11416" class="Keyword">infixr</a> <a id="11423" class="Number">6</a> <a id="11425" href="Basics.html#11516" class="Function Operator">_&amp;&amp;_</a>
<a id="11430" class="Keyword">infixr</a> <a id="11437" class="Number">5</a> <a id="11439" href="Basics.html#11580" class="Function Operator">_||_</a>

</pre>Negation
<pre class="Agda">
<a id="~_"></a><a id="11463" href="Basics.html#11463" class="Function Operator">~_</a> <a id="11466" class="Symbol">:</a> <a id="11468" href="Basics.html#10888" class="Datatype">𝔹</a> <a id="11470" class="Symbol">→</a> <a id="11472" href="Basics.html#10888" class="Datatype">𝔹</a>
<a id="11474" href="Basics.html#11463" class="Function Operator">~</a> <a id="11476" href="Basics.html#10904" class="InductiveConstructor">tt</a> <a id="11479" class="Symbol">=</a> <a id="11481" href="Basics.html#10907" class="InductiveConstructor">ff</a>
<a id="11484" href="Basics.html#11463" class="Function Operator">~</a> <a id="11486" href="Basics.html#10907" class="InductiveConstructor">ff</a> <a id="11489" class="Symbol">=</a> <a id="11491" href="Basics.html#10904" class="InductiveConstructor">tt</a>

</pre>Conjunction
<pre class="Agda">
<a id="_&amp;&amp;_"></a><a id="11516" href="Basics.html#11516" class="Function Operator">_&amp;&amp;_</a> <a id="11521" class="Symbol">:</a> <a id="11523" href="Basics.html#10888" class="Datatype">𝔹</a> <a id="11525" class="Symbol">→</a> <a id="11527" href="Basics.html#10888" class="Datatype">𝔹</a> <a id="11529" class="Symbol">→</a> <a id="11531" href="Basics.html#10888" class="Datatype">𝔹</a>
<a id="11533" href="Basics.html#10904" class="InductiveConstructor">tt</a> <a id="11536" href="Basics.html#11516" class="Function Operator">&amp;&amp;</a> <a id="11539" href="Basics.html#11539" class="Bound">b</a> <a id="11541" class="Symbol">=</a> <a id="11543" href="Basics.html#11539" class="Bound">b</a>
<a id="11545" href="Basics.html#10907" class="InductiveConstructor">ff</a> <a id="11548" href="Basics.html#11516" class="Function Operator">&amp;&amp;</a> <a id="11551" class="Symbol">_</a> <a id="11553" class="Symbol">=</a> <a id="11555" href="Basics.html#10907" class="InductiveConstructor">ff</a>

</pre>Disjunction
<pre class="Agda">
<a id="_||_"></a><a id="11580" href="Basics.html#11580" class="Function Operator">_||_</a> <a id="11585" class="Symbol">:</a> <a id="11587" href="Basics.html#10888" class="Datatype">𝔹</a> <a id="11589" class="Symbol">→</a> <a id="11591" href="Basics.html#10888" class="Datatype">𝔹</a> <a id="11593" class="Symbol">→</a> <a id="11595" href="Basics.html#10888" class="Datatype">𝔹</a>
<a id="11597" href="Basics.html#10904" class="InductiveConstructor">tt</a> <a id="11600" href="Basics.html#11580" class="Function Operator">||</a> <a id="11603" class="Symbol">_</a> <a id="11605" class="Symbol">=</a> <a id="11607" href="Basics.html#10904" class="InductiveConstructor">tt</a>
<a id="11610" href="Basics.html#10907" class="InductiveConstructor">ff</a> <a id="11613" href="Basics.html#11580" class="Function Operator">||</a> <a id="11616" href="Basics.html#11616" class="Bound">b</a> <a id="11618" class="Symbol">=</a> <a id="11620" href="Basics.html#11616" class="Bound">b</a>

</pre>Implication
<pre class="Agda">
<a id="_imp_"></a><a id="11644" href="Basics.html#11644" class="Function Operator">_imp_</a> <a id="11650" class="Symbol">:</a> <a id="11652" href="Basics.html#10888" class="Datatype">𝔹</a> <a id="11654" class="Symbol">→</a> <a id="11656" href="Basics.html#10888" class="Datatype">𝔹</a> <a id="11658" class="Symbol">→</a> <a id="11660" href="Basics.html#10888" class="Datatype">𝔹</a>
<a id="11662" href="Basics.html#10904" class="InductiveConstructor">tt</a> <a id="11665" href="Basics.html#11644" class="Function Operator">imp</a> <a id="11669" href="Basics.html#10904" class="InductiveConstructor">tt</a> <a id="11672" class="Symbol">=</a> <a id="11674" href="Basics.html#10904" class="InductiveConstructor">tt</a>
<a id="11677" href="Basics.html#10904" class="InductiveConstructor">tt</a> <a id="11680" href="Basics.html#11644" class="Function Operator">imp</a> <a id="11684" href="Basics.html#10907" class="InductiveConstructor">ff</a> <a id="11687" class="Symbol">=</a> <a id="11689" href="Basics.html#10907" class="InductiveConstructor">ff</a>
<a id="11692" href="Basics.html#10907" class="InductiveConstructor">ff</a> <a id="11695" href="Basics.html#11644" class="Function Operator">imp</a> <a id="11699" class="Symbol">_</a> <a id="11701" class="Symbol">=</a> <a id="11703" href="Basics.html#10904" class="InductiveConstructor">tt</a>

</pre>Exclusive Or
<pre class="Agda">
<a id="_xor_"></a><a id="11729" href="Basics.html#11729" class="Function Operator">_xor_</a> <a id="11735" class="Symbol">:</a> <a id="11737" href="Basics.html#10888" class="Datatype">𝔹</a> <a id="11739" class="Symbol">→</a> <a id="11741" href="Basics.html#10888" class="Datatype">𝔹</a> <a id="11743" class="Symbol">→</a> <a id="11745" href="Basics.html#10888" class="Datatype">𝔹</a>
<a id="11747" href="Basics.html#10904" class="InductiveConstructor">tt</a> <a id="11750" href="Basics.html#11729" class="Function Operator">xor</a> <a id="11754" href="Basics.html#10904" class="InductiveConstructor">tt</a> <a id="11757" class="Symbol">=</a> <a id="11759" href="Basics.html#10907" class="InductiveConstructor">ff</a>
<a id="11762" href="Basics.html#10904" class="InductiveConstructor">tt</a> <a id="11765" href="Basics.html#11729" class="Function Operator">xor</a> <a id="11769" href="Basics.html#10907" class="InductiveConstructor">ff</a> <a id="11772" class="Symbol">=</a> <a id="11774" href="Basics.html#10904" class="InductiveConstructor">tt</a>
<a id="11777" href="Basics.html#10907" class="InductiveConstructor">ff</a> <a id="11780" href="Basics.html#11729" class="Function Operator">xor</a> <a id="11784" href="Basics.html#10904" class="InductiveConstructor">tt</a> <a id="11787" class="Symbol">=</a> <a id="11789" href="Basics.html#10904" class="InductiveConstructor">tt</a>
<a id="11792" href="Basics.html#10907" class="InductiveConstructor">ff</a> <a id="11795" href="Basics.html#11729" class="Function Operator">xor</a> <a id="11799" href="Basics.html#10907" class="InductiveConstructor">ff</a> <a id="11802" class="Symbol">=</a> <a id="11804" href="Basics.html#10907" class="InductiveConstructor">ff</a>

</pre>Nand gate
<pre class="Agda">
<a id="_nand_"></a><a id="11827" href="Basics.html#11827" class="Function Operator">_nand_</a> <a id="11834" class="Symbol">:</a> <a id="11836" href="Basics.html#10888" class="Datatype">𝔹</a> <a id="11838" class="Symbol">→</a> <a id="11840" href="Basics.html#10888" class="Datatype">𝔹</a> <a id="11842" class="Symbol">→</a> <a id="11844" href="Basics.html#10888" class="Datatype">𝔹</a>
<a id="11846" href="Basics.html#10904" class="InductiveConstructor">tt</a> <a id="11849" href="Basics.html#11827" class="Function Operator">nand</a> <a id="11854" href="Basics.html#10904" class="InductiveConstructor">tt</a> <a id="11857" class="Symbol">=</a> <a id="11859" href="Basics.html#10907" class="InductiveConstructor">ff</a>
<a id="11862" href="Basics.html#10904" class="InductiveConstructor">tt</a> <a id="11865" href="Basics.html#11827" class="Function Operator">nand</a> <a id="11870" href="Basics.html#10907" class="InductiveConstructor">ff</a> <a id="11873" class="Symbol">=</a> <a id="11875" href="Basics.html#10904" class="InductiveConstructor">tt</a>
<a id="11878" href="Basics.html#10907" class="InductiveConstructor">ff</a> <a id="11881" href="Basics.html#11827" class="Function Operator">nand</a> <a id="11886" class="Symbol">_</a> <a id="11888" class="Symbol">=</a> <a id="11890" href="Basics.html#10904" class="InductiveConstructor">tt</a>

</pre>  Declaring associativity and precedence allows us to write
  `~ a && b && ~ c` instead of `(~ a) && (b && (~ c))`.
  We choose right associativity for `_||_` and `_&&_` so
  they can "short-circuit" properly when chained. 

  We then define the more complicated `Day` abstract data type
  that appears in every tutorial like this one in order to keep
  up the tradition.
<pre class="Agda">
<a id="12275" class="Keyword">data</a> <a id="Day"></a><a id="12280" href="Basics.html#12280" class="Datatype">Day</a> <a id="12284" class="Symbol">:</a> <a id="12286" class="PrimitiveType">Set</a> <a id="12290" class="Keyword">where</a>
  <a id="Day.Monday"></a><a id="12298" href="Basics.html#12298" class="InductiveConstructor">Monday</a> <a id="Day.Tuesday"></a><a id="12305" href="Basics.html#12305" class="InductiveConstructor">Tuesday</a> <a id="Day.Wednesday"></a><a id="12313" href="Basics.html#12313" class="InductiveConstructor">Wednesday</a> <a id="Day.Thursday"></a><a id="12323" href="Basics.html#12323" class="InductiveConstructor">Thursday</a> <a id="Day.Friday"></a><a id="12332" href="Basics.html#12332" class="InductiveConstructor">Friday</a> <a id="Day.Saturday"></a><a id="12339" href="Basics.html#12339" class="InductiveConstructor">Saturday</a> <a id="Day.Sunday"></a><a id="12348" href="Basics.html#12348" class="InductiveConstructor">Sunday</a> <a id="12355" class="Symbol">:</a> <a id="12357" href="Basics.html#12280" class="Datatype">Day</a>

</pre>  We also define a function that returns the next business day.
  Its main purpose is to install great thanks in us.
  Imagine writing the below with some god-awful inductive principle
  involving eight unlabeled arguments instead of pattern matching.
<pre class="Agda">
<a id="next-business-day"></a><a id="12623" href="Basics.html#12623" class="Function">next-business-day</a> <a id="12641" class="Symbol">:</a> <a id="12643" href="Basics.html#12280" class="Datatype">Day</a> <a id="12647" class="Symbol">→</a> <a id="12649" href="Basics.html#12280" class="Datatype">Day</a>
<a id="12653" href="Basics.html#12623" class="Function">next-business-day</a> <a id="12671" href="Basics.html#12298" class="InductiveConstructor">Monday</a> <a id="12678" class="Symbol">=</a> <a id="12680" href="Basics.html#12305" class="InductiveConstructor">Tuesday</a>
<a id="12688" href="Basics.html#12623" class="Function">next-business-day</a> <a id="12706" href="Basics.html#12305" class="InductiveConstructor">Tuesday</a> <a id="12714" class="Symbol">=</a> <a id="12716" href="Basics.html#12313" class="InductiveConstructor">Wednesday</a>
<a id="12726" href="Basics.html#12623" class="Function">next-business-day</a> <a id="12744" href="Basics.html#12313" class="InductiveConstructor">Wednesday</a> <a id="12754" class="Symbol">=</a> <a id="12756" href="Basics.html#12323" class="InductiveConstructor">Thursday</a>
<a id="12765" href="Basics.html#12623" class="Function">next-business-day</a> <a id="12783" href="Basics.html#12323" class="InductiveConstructor">Thursday</a> <a id="12792" class="Symbol">=</a> <a id="12794" href="Basics.html#12332" class="InductiveConstructor">Friday</a>
<a id="12801" href="Basics.html#12623" class="Function">next-business-day</a> <a id="12819" href="Basics.html#12332" class="InductiveConstructor">Friday</a> <a id="12826" class="Symbol">=</a> <a id="12828" href="Basics.html#12298" class="InductiveConstructor">Monday</a>
<a id="12835" href="Basics.html#12623" class="Function">next-business-day</a> <a id="12853" href="Basics.html#12339" class="InductiveConstructor">Saturday</a> <a id="12862" class="Symbol">=</a> <a id="12864" href="Basics.html#12298" class="InductiveConstructor">Monday</a>
<a id="12871" href="Basics.html#12623" class="Function">next-business-day</a> <a id="12889" href="Basics.html#12348" class="InductiveConstructor">Sunday</a> <a id="12896" class="Symbol">=</a> <a id="12898" href="Basics.html#12298" class="InductiveConstructor">Monday</a>

</pre>  ...and some fun with colors and inductive data types,
  which depend on other inductive data types...
<pre class="Agda">
<a id="13019" class="Keyword">data</a> <a id="RYB"></a><a id="13024" href="Basics.html#13024" class="Datatype">RYB</a> <a id="13028" class="Symbol">:</a> <a id="13030" class="PrimitiveType">Set</a> <a id="13034" class="Keyword">where</a>
  <a id="RYB.Red"></a><a id="13042" href="Basics.html#13042" class="InductiveConstructor">Red</a> <a id="RYB.Yellow"></a><a id="13046" href="Basics.html#13046" class="InductiveConstructor">Yellow</a> <a id="RYB.Blue"></a><a id="13053" href="Basics.html#13053" class="InductiveConstructor">Blue</a> <a id="13058" class="Symbol">:</a> <a id="13060" href="Basics.html#13024" class="Datatype">RYB</a>

<a id="13065" class="Keyword">data</a> <a id="OGV"></a><a id="13070" href="Basics.html#13070" class="Datatype">OGV</a> <a id="13074" class="Symbol">:</a> <a id="13076" class="PrimitiveType">Set</a> <a id="13080" class="Keyword">where</a>
  <a id="OGV.Orange"></a><a id="13088" href="Basics.html#13088" class="InductiveConstructor">Orange</a> <a id="OGV.Green"></a><a id="13095" href="Basics.html#13095" class="InductiveConstructor">Green</a> <a id="OGV.Violet"></a><a id="13101" href="Basics.html#13101" class="InductiveConstructor">Violet</a> <a id="13108" class="Symbol">:</a> <a id="13110" href="Basics.html#13070" class="Datatype">OGV</a>

<a id="13115" class="Keyword">data</a> <a id="Color"></a><a id="13120" href="Basics.html#13120" class="Datatype">Color</a> <a id="13126" class="Symbol">:</a> <a id="13128" class="PrimitiveType">Set</a> <a id="13132" class="Keyword">where</a>
  <a id="Color.Black"></a><a id="13140" href="Basics.html#13140" class="InductiveConstructor">Black</a> <a id="Color.White"></a><a id="13146" href="Basics.html#13146" class="InductiveConstructor">White</a> <a id="13152" class="Symbol">:</a> <a id="13154" href="Basics.html#13120" class="Datatype">Color</a>
  <a id="Color.Primary"></a><a id="13162" href="Basics.html#13162" class="InductiveConstructor">Primary</a> <a id="13170" class="Symbol">:</a> <a id="13172" href="Basics.html#13024" class="Datatype">RYB</a> <a id="13176" class="Symbol">→</a> <a id="13178" href="Basics.html#13120" class="Datatype">Color</a>
  <a id="Color.Secondary"></a><a id="13186" href="Basics.html#13186" class="InductiveConstructor">Secondary</a> <a id="13196" class="Symbol">:</a> <a id="13198" href="Basics.html#13070" class="Datatype">OGV</a> <a id="13202" class="Symbol">→</a> <a id="13204" href="Basics.html#13120" class="Datatype">Color</a>

<a id="is-red"></a><a id="13211" href="Basics.html#13211" class="Function">is-red</a> <a id="13218" class="Symbol">:</a> <a id="13220" href="Basics.html#13120" class="Datatype">Color</a> <a id="13226" class="Symbol">→</a> <a id="13228" href="Basics.html#10888" class="Datatype">𝔹</a>
<a id="13230" href="Basics.html#13211" class="Function">is-red</a> <a id="13237" href="Basics.html#13140" class="InductiveConstructor">Black</a> <a id="13243" class="Symbol">=</a> <a id="13245" href="Basics.html#10907" class="InductiveConstructor">ff</a>
<a id="13248" href="Basics.html#13211" class="Function">is-red</a> <a id="13255" href="Basics.html#13146" class="InductiveConstructor">White</a> <a id="13261" class="Symbol">=</a> <a id="13263" href="Basics.html#10907" class="InductiveConstructor">ff</a>
<a id="13266" href="Basics.html#13211" class="Function">is-red</a> <a id="13273" class="Symbol">(</a><a id="13274" href="Basics.html#13162" class="InductiveConstructor">Primary</a> <a id="13282" href="Basics.html#13042" class="InductiveConstructor">Red</a><a id="13285" class="Symbol">)</a> <a id="13287" class="Symbol">=</a> <a id="13289" href="Basics.html#10904" class="InductiveConstructor">tt</a>
<a id="13292" href="Basics.html#13211" class="Function">is-red</a> <a id="13299" class="Symbol">(</a><a id="13300" href="Basics.html#13162" class="InductiveConstructor">Primary</a> <a id="13308" href="Basics.html#13046" class="InductiveConstructor">Yellow</a><a id="13314" class="Symbol">)</a> <a id="13316" class="Symbol">=</a> <a id="13318" href="Basics.html#10907" class="InductiveConstructor">ff</a>
<a id="13321" href="Basics.html#13211" class="Function">is-red</a> <a id="13328" class="Symbol">(</a><a id="13329" href="Basics.html#13162" class="InductiveConstructor">Primary</a> <a id="13337" href="Basics.html#13053" class="InductiveConstructor">Blue</a><a id="13341" class="Symbol">)</a> <a id="13343" class="Symbol">=</a> <a id="13345" href="Basics.html#10907" class="InductiveConstructor">ff</a>
<a id="13348" href="Basics.html#13211" class="Function">is-red</a> <a id="13355" class="Symbol">(</a><a id="13356" href="Basics.html#13186" class="InductiveConstructor">Secondary</a> <a id="13366" class="Symbol">_)</a> <a id="13369" class="Symbol">=</a> <a id="13371" href="Basics.html#10907" class="InductiveConstructor">ff</a>

</pre>  We almost have enough to start proving some rudimentary theorems about
  Booleans.
  What we are missing is a notion of equality.
  The notion of equality, expressed in terms of the `Id` type is one of the
  cornerstones of UF. 
  
<pre class="Agda">
<a id="13618" class="Keyword">data</a> <a id="Id"></a><a id="13623" href="Basics.html#13623" class="Datatype">Id</a> <a id="13626" class="Symbol">(</a><a id="13627" href="Basics.html#13627" class="Bound">X</a> <a id="13629" class="Symbol">:</a> <a id="13631" class="PrimitiveType">Set</a> <a id="13635" href="Basics.html#6481" class="Generalizable">ℓ</a><a id="13636" class="Symbol">)</a> <a id="13638" class="Symbol">:</a> <a id="13640" href="Basics.html#13627" class="Bound">X</a> <a id="13642" class="Symbol">→</a> <a id="13644" href="Basics.html#13627" class="Bound">X</a> <a id="13646" class="Symbol">→</a> <a id="13648" class="PrimitiveType">Set</a> <a id="13652" href="Basics.html#13635" class="Bound">ℓ</a> <a id="13654" class="Keyword">where</a>
  <a id="Id.refl"></a><a id="13662" href="Basics.html#13662" class="InductiveConstructor">refl</a> <a id="13667" class="Symbol">:</a> <a id="13669" class="Symbol">(</a><a id="13670" href="Basics.html#13670" class="Bound">x</a> <a id="13672" class="Symbol">:</a> <a id="13674" href="Basics.html#13627" class="Bound">X</a><a id="13675" class="Symbol">)</a> <a id="13677" class="Symbol">→</a> <a id="13679" href="Basics.html#13623" class="Datatype">Id</a> <a id="13682" href="Basics.html#13627" class="Bound">X</a> <a id="13684" href="Basics.html#13670" class="Bound">x</a> <a id="13686" href="Basics.html#13670" class="Bound">x</a>

<a id="_≡_"></a><a id="13689" href="Basics.html#13689" class="Function Operator">_≡_</a> <a id="13693" class="Symbol">:</a> <a id="13695" class="Symbol">{</a><a id="13696" href="Basics.html#13696" class="Bound">X</a> <a id="13698" class="Symbol">:</a> <a id="13700" class="PrimitiveType">Set</a> <a id="13704" href="Basics.html#6481" class="Generalizable">ℓ</a><a id="13705" class="Symbol">}</a> <a id="13707" class="Symbol">→</a> <a id="13709" href="Basics.html#13696" class="Bound">X</a> <a id="13711" class="Symbol">→</a> <a id="13713" href="Basics.html#13696" class="Bound">X</a> <a id="13715" class="Symbol">→</a> <a id="13717" class="PrimitiveType">Set</a> <a id="13721" href="Basics.html#6481" class="Generalizable">ℓ</a>
<a id="13723" href="Basics.html#13723" class="Bound">x</a> <a id="13725" href="Basics.html#13689" class="Function Operator">≡</a> <a id="13727" href="Basics.html#13727" class="Bound">y</a> <a id="13729" class="Symbol">=</a> <a id="13731" href="Basics.html#13623" class="Datatype">Id</a> <a id="13734" class="Symbol">_</a> <a id="13736" href="Basics.html#13723" class="Bound">x</a> <a id="13738" href="Basics.html#13727" class="Bound">y</a>

<a id="_≢_"></a><a id="13741" href="Basics.html#13741" class="Function Operator">_≢_</a> <a id="13745" class="Symbol">:</a> <a id="13747" class="Symbol">{</a><a id="13748" href="Basics.html#13748" class="Bound">X</a> <a id="13750" class="Symbol">:</a> <a id="13752" class="PrimitiveType">Set</a> <a id="13756" href="Basics.html#6481" class="Generalizable">ℓ</a><a id="13757" class="Symbol">}</a> <a id="13759" class="Symbol">→</a> <a id="13761" href="Basics.html#13748" class="Bound">X</a> <a id="13763" class="Symbol">→</a> <a id="13765" href="Basics.html#13748" class="Bound">X</a> <a id="13767" class="Symbol">→</a> <a id="13769" class="PrimitiveType">Set</a> <a id="13773" href="Basics.html#6481" class="Generalizable">ℓ</a>
<a id="13775" href="Basics.html#13775" class="Bound">x</a> <a id="13777" href="Basics.html#13741" class="Function Operator">≢</a> <a id="13779" href="Basics.html#13779" class="Bound">y</a> <a id="13781" class="Symbol">=</a> <a id="13783" href="Basics.html#10760" class="Function">¬</a> <a id="13785" class="Symbol">(</a><a id="13786" href="Basics.html#13775" class="Bound">x</a> <a id="13788" href="Basics.html#13689" class="Function Operator">≡</a> <a id="13790" href="Basics.html#13779" class="Bound">y</a><a id="13791" class="Symbol">)</a>

</pre>  The definition seems simple but nuance will be revealed when we start
  reasoning with the form of equality, argue with its induction
  principle, and define the notion of transport.
  But for the type of statements we will prove now, we can just use
  pattern matching and evaluation. 

  For now, we should congratulate ourselves on defining our first data-type
  parametrized/indexed by elements of a type.
  This sort of type can't be expressed in weaker type systems.

  We define `_≡_` and `_≢_` for notational convenience. 

  Our first theorem is that `~_` is an involution, i.e., '~_` applied
  twice is the same as the identity operator. 
<pre class="Agda">

<a id="~-involutive"></a><a id="14455" href="Basics.html#14455" class="Function">~-involutive</a> <a id="14468" class="Symbol">:</a> <a id="14470" class="Symbol">(</a><a id="14471" href="Basics.html#14471" class="Bound">b</a> <a id="14473" class="Symbol">:</a> <a id="14475" href="Basics.html#10888" class="Datatype">𝔹</a><a id="14476" class="Symbol">)</a> <a id="14478" class="Symbol">→</a> <a id="14480" class="Symbol">(</a><a id="14481" href="Basics.html#11463" class="Function Operator">~</a> <a id="14483" class="Symbol">(</a><a id="14484" href="Basics.html#11463" class="Function Operator">~</a> <a id="14486" href="Basics.html#14471" class="Bound">b</a><a id="14487" class="Symbol">))</a> <a id="14490" href="Basics.html#13689" class="Function Operator">≡</a> <a id="14492" href="Basics.html#14471" class="Bound">b</a>
<a id="14494" href="Basics.html#14455" class="Function">~-involutive</a> <a id="14507" href="Basics.html#10904" class="InductiveConstructor">tt</a> <a id="14510" class="Symbol">=</a> <a id="14512" href="Basics.html#13662" class="InductiveConstructor">refl</a> <a id="14517" class="Symbol">_</a>
<a id="14519" href="Basics.html#14455" class="Function">~-involutive</a> <a id="14532" href="Basics.html#10907" class="InductiveConstructor">ff</a> <a id="14535" class="Symbol">=</a> <a id="14537" href="Basics.html#13662" class="InductiveConstructor">refl</a> <a id="14542" class="Symbol">_</a>

</pre>  We can prove that a Boolean "xored" with itself always return false. 
<pre class="Agda">
<a id="xor-anti-refl"></a><a id="14626" href="Basics.html#14626" class="Function">xor-anti-refl</a> <a id="14640" class="Symbol">:</a> <a id="14642" class="Symbol">(</a><a id="14643" href="Basics.html#14643" class="Bound">b</a> <a id="14645" class="Symbol">:</a> <a id="14647" href="Basics.html#10888" class="Datatype">𝔹</a><a id="14648" class="Symbol">)</a> <a id="14650" class="Symbol">→</a> <a id="14652" class="Symbol">(</a><a id="14653" href="Basics.html#14643" class="Bound">b</a> <a id="14655" href="Basics.html#11729" class="Function Operator">xor</a> <a id="14659" href="Basics.html#14643" class="Bound">b</a><a id="14660" class="Symbol">)</a> <a id="14662" href="Basics.html#13689" class="Function Operator">≡</a> <a id="14664" href="Basics.html#10907" class="InductiveConstructor">ff</a>
<a id="14667" href="Basics.html#14626" class="Function">xor-anti-refl</a> <a id="14681" href="Basics.html#10904" class="InductiveConstructor">tt</a> <a id="14684" class="Symbol">=</a> <a id="14686" href="Basics.html#13662" class="InductiveConstructor">refl</a> <a id="14691" class="Symbol">_</a>
<a id="14693" href="Basics.html#14626" class="Function">xor-anti-refl</a> <a id="14707" href="Basics.html#10907" class="InductiveConstructor">ff</a> <a id="14710" class="Symbol">=</a> <a id="14712" href="Basics.html#13662" class="InductiveConstructor">refl</a> <a id="14717" class="Symbol">_</a>

</pre>  We can prove that logical implication can be expressed in
  terms of `_||_` and `~_`
<pre class="Agda">
<a id="imp-def"></a><a id="14816" href="Basics.html#14816" class="Function">imp-def</a> <a id="14824" class="Symbol">:</a> <a id="14826" class="Symbol">(</a><a id="14827" href="Basics.html#14827" class="Bound">b</a> <a id="14829" href="Basics.html#14829" class="Bound">c</a> <a id="14831" class="Symbol">:</a> <a id="14833" href="Basics.html#10888" class="Datatype">𝔹</a><a id="14834" class="Symbol">)</a> <a id="14836" class="Symbol">→</a> <a id="14838" class="Symbol">(</a><a id="14839" href="Basics.html#14827" class="Bound">b</a> <a id="14841" href="Basics.html#11644" class="Function Operator">imp</a> <a id="14845" href="Basics.html#14829" class="Bound">c</a><a id="14846" class="Symbol">)</a> <a id="14848" href="Basics.html#13689" class="Function Operator">≡</a> <a id="14850" class="Symbol">(</a><a id="14851" href="Basics.html#11463" class="Function Operator">~</a> <a id="14853" href="Basics.html#14827" class="Bound">b</a> <a id="14855" href="Basics.html#11580" class="Function Operator">||</a> <a id="14858" href="Basics.html#14829" class="Bound">c</a><a id="14859" class="Symbol">)</a>
<a id="14861" href="Basics.html#14816" class="Function">imp-def</a> <a id="14869" href="Basics.html#10904" class="InductiveConstructor">tt</a> <a id="14872" href="Basics.html#10904" class="InductiveConstructor">tt</a> <a id="14875" class="Symbol">=</a> <a id="14877" href="Basics.html#13662" class="InductiveConstructor">refl</a> <a id="14882" class="Symbol">_</a>
<a id="14884" href="Basics.html#14816" class="Function">imp-def</a> <a id="14892" href="Basics.html#10904" class="InductiveConstructor">tt</a> <a id="14895" href="Basics.html#10907" class="InductiveConstructor">ff</a> <a id="14898" class="Symbol">=</a> <a id="14900" href="Basics.html#13662" class="InductiveConstructor">refl</a> <a id="14905" class="Symbol">_</a>
<a id="14907" href="Basics.html#14816" class="Function">imp-def</a> <a id="14915" href="Basics.html#10907" class="InductiveConstructor">ff</a> <a id="14918" href="Basics.html#14918" class="Bound">c</a> <a id="14920" class="Symbol">=</a> <a id="14922" href="Basics.html#13662" class="InductiveConstructor">refl</a> <a id="14927" class="Symbol">_</a>

</pre>  `refl _` is the closest we can say to "because obviously" in Agda.
  The proof `refl _` implies that two things are equal by definition.  

  We also prove commutative and associative properties of `_&&_` and `_||_`.
<pre class="Agda">
<a id="&amp;&amp;-comm"></a><a id="15158" href="Basics.html#15158" class="Function">&amp;&amp;-comm</a> <a id="15166" class="Symbol">:</a> <a id="15168" class="Symbol">(</a><a id="15169" href="Basics.html#15169" class="Bound">b</a> <a id="15171" href="Basics.html#15171" class="Bound">c</a> <a id="15173" class="Symbol">:</a> <a id="15175" href="Basics.html#10888" class="Datatype">𝔹</a><a id="15176" class="Symbol">)</a> <a id="15178" class="Symbol">→</a> <a id="15180" class="Symbol">(</a><a id="15181" href="Basics.html#15171" class="Bound">c</a> <a id="15183" href="Basics.html#11516" class="Function Operator">&amp;&amp;</a> <a id="15186" href="Basics.html#15169" class="Bound">b</a><a id="15187" class="Symbol">)</a> <a id="15189" href="Basics.html#13689" class="Function Operator">≡</a> <a id="15191" class="Symbol">(</a><a id="15192" href="Basics.html#15169" class="Bound">b</a> <a id="15194" href="Basics.html#11516" class="Function Operator">&amp;&amp;</a> <a id="15197" href="Basics.html#15171" class="Bound">c</a><a id="15198" class="Symbol">)</a>
<a id="15200" href="Basics.html#15158" class="Function">&amp;&amp;-comm</a> <a id="15208" href="Basics.html#10904" class="InductiveConstructor">tt</a> <a id="15211" href="Basics.html#10904" class="InductiveConstructor">tt</a> <a id="15214" class="Symbol">=</a> <a id="15216" href="Basics.html#13662" class="InductiveConstructor">refl</a> <a id="15221" class="Symbol">_</a>
<a id="15223" href="Basics.html#15158" class="Function">&amp;&amp;-comm</a> <a id="15231" href="Basics.html#10904" class="InductiveConstructor">tt</a> <a id="15234" href="Basics.html#10907" class="InductiveConstructor">ff</a> <a id="15237" class="Symbol">=</a> <a id="15239" href="Basics.html#13662" class="InductiveConstructor">refl</a> <a id="15244" class="Symbol">_</a>
<a id="15246" href="Basics.html#15158" class="Function">&amp;&amp;-comm</a> <a id="15254" href="Basics.html#10907" class="InductiveConstructor">ff</a> <a id="15257" href="Basics.html#10904" class="InductiveConstructor">tt</a> <a id="15260" class="Symbol">=</a> <a id="15262" href="Basics.html#13662" class="InductiveConstructor">refl</a> <a id="15267" class="Symbol">_</a>
<a id="15269" href="Basics.html#15158" class="Function">&amp;&amp;-comm</a> <a id="15277" href="Basics.html#10907" class="InductiveConstructor">ff</a> <a id="15280" href="Basics.html#10907" class="InductiveConstructor">ff</a> <a id="15283" class="Symbol">=</a> <a id="15285" href="Basics.html#13662" class="InductiveConstructor">refl</a> <a id="15290" class="Symbol">_</a>

<a id="||-comm"></a><a id="15293" href="Basics.html#15293" class="Function">||-comm</a> <a id="15301" class="Symbol">:</a> <a id="15303" class="Symbol">(</a><a id="15304" href="Basics.html#15304" class="Bound">b</a> <a id="15306" href="Basics.html#15306" class="Bound">c</a> <a id="15308" class="Symbol">:</a> <a id="15310" href="Basics.html#10888" class="Datatype">𝔹</a><a id="15311" class="Symbol">)</a> <a id="15313" class="Symbol">→</a> <a id="15315" class="Symbol">(</a><a id="15316" href="Basics.html#15306" class="Bound">c</a> <a id="15318" href="Basics.html#11580" class="Function Operator">||</a> <a id="15321" href="Basics.html#15304" class="Bound">b</a><a id="15322" class="Symbol">)</a> <a id="15324" href="Basics.html#13689" class="Function Operator">≡</a> <a id="15326" class="Symbol">(</a><a id="15327" href="Basics.html#15304" class="Bound">b</a> <a id="15329" href="Basics.html#11580" class="Function Operator">||</a> <a id="15332" href="Basics.html#15306" class="Bound">c</a><a id="15333" class="Symbol">)</a>
<a id="15335" href="Basics.html#15293" class="Function">||-comm</a> <a id="15343" href="Basics.html#10904" class="InductiveConstructor">tt</a> <a id="15346" href="Basics.html#10904" class="InductiveConstructor">tt</a> <a id="15349" class="Symbol">=</a> <a id="15351" href="Basics.html#13662" class="InductiveConstructor">refl</a> <a id="15356" class="Symbol">_</a>
<a id="15358" href="Basics.html#15293" class="Function">||-comm</a> <a id="15366" href="Basics.html#10904" class="InductiveConstructor">tt</a> <a id="15369" href="Basics.html#10907" class="InductiveConstructor">ff</a> <a id="15372" class="Symbol">=</a> <a id="15374" href="Basics.html#13662" class="InductiveConstructor">refl</a> <a id="15379" class="Symbol">_</a>
<a id="15381" href="Basics.html#15293" class="Function">||-comm</a> <a id="15389" href="Basics.html#10907" class="InductiveConstructor">ff</a> <a id="15392" href="Basics.html#10904" class="InductiveConstructor">tt</a> <a id="15395" class="Symbol">=</a> <a id="15397" href="Basics.html#13662" class="InductiveConstructor">refl</a> <a id="15402" class="Symbol">_</a>
<a id="15404" href="Basics.html#15293" class="Function">||-comm</a> <a id="15412" href="Basics.html#10907" class="InductiveConstructor">ff</a> <a id="15415" href="Basics.html#10907" class="InductiveConstructor">ff</a> <a id="15418" class="Symbol">=</a> <a id="15420" href="Basics.html#13662" class="InductiveConstructor">refl</a> <a id="15425" class="Symbol">_</a>

</pre>  Reminder:  `b && c && d` is parsed as `b && (c && d)`
<pre class="Agda">

<a id="&amp;&amp;-assoc"></a><a id="15494" href="Basics.html#15494" class="Function">&amp;&amp;-assoc</a> <a id="15503" class="Symbol">:</a> <a id="15505" class="Symbol">(</a><a id="15506" href="Basics.html#15506" class="Bound">b</a> <a id="15508" href="Basics.html#15508" class="Bound">c</a> <a id="15510" href="Basics.html#15510" class="Bound">d</a> <a id="15512" class="Symbol">:</a> <a id="15514" href="Basics.html#10888" class="Datatype">𝔹</a><a id="15515" class="Symbol">)</a> <a id="15517" class="Symbol">→</a> <a id="15519" class="Symbol">((</a><a id="15521" href="Basics.html#15506" class="Bound">b</a> <a id="15523" href="Basics.html#11516" class="Function Operator">&amp;&amp;</a> <a id="15526" href="Basics.html#15508" class="Bound">c</a><a id="15527" class="Symbol">)</a> <a id="15529" href="Basics.html#11516" class="Function Operator">&amp;&amp;</a> <a id="15532" href="Basics.html#15510" class="Bound">d</a><a id="15533" class="Symbol">)</a> <a id="15535" href="Basics.html#13689" class="Function Operator">≡</a> <a id="15537" class="Symbol">(</a><a id="15538" href="Basics.html#15506" class="Bound">b</a> <a id="15540" href="Basics.html#11516" class="Function Operator">&amp;&amp;</a> <a id="15543" href="Basics.html#15508" class="Bound">c</a> <a id="15545" href="Basics.html#11516" class="Function Operator">&amp;&amp;</a> <a id="15548" href="Basics.html#15510" class="Bound">d</a><a id="15549" class="Symbol">)</a>
<a id="15551" href="Basics.html#15494" class="Function">&amp;&amp;-assoc</a> <a id="15560" href="Basics.html#10904" class="InductiveConstructor">tt</a> <a id="15563" href="Basics.html#15563" class="Bound">c</a> <a id="15565" href="Basics.html#15565" class="Bound">d</a> <a id="15567" class="Symbol">=</a> <a id="15569" href="Basics.html#13662" class="InductiveConstructor">refl</a> <a id="15574" class="Symbol">_</a>
<a id="15576" href="Basics.html#15494" class="Function">&amp;&amp;-assoc</a> <a id="15585" href="Basics.html#10907" class="InductiveConstructor">ff</a> <a id="15588" href="Basics.html#15588" class="Bound">c</a> <a id="15590" href="Basics.html#15590" class="Bound">d</a> <a id="15592" class="Symbol">=</a> <a id="15594" href="Basics.html#13662" class="InductiveConstructor">refl</a> <a id="15599" class="Symbol">_</a>

<a id="||-assoc"></a><a id="15602" href="Basics.html#15602" class="Function">||-assoc</a> <a id="15611" class="Symbol">:</a> <a id="15613" class="Symbol">(</a><a id="15614" href="Basics.html#15614" class="Bound">b</a> <a id="15616" href="Basics.html#15616" class="Bound">c</a> <a id="15618" href="Basics.html#15618" class="Bound">d</a> <a id="15620" class="Symbol">:</a> <a id="15622" href="Basics.html#10888" class="Datatype">𝔹</a><a id="15623" class="Symbol">)</a> <a id="15625" class="Symbol">→</a> <a id="15627" class="Symbol">((</a><a id="15629" href="Basics.html#15614" class="Bound">b</a> <a id="15631" href="Basics.html#11580" class="Function Operator">||</a> <a id="15634" href="Basics.html#15616" class="Bound">c</a><a id="15635" class="Symbol">)</a> <a id="15637" href="Basics.html#11580" class="Function Operator">||</a> <a id="15640" href="Basics.html#15618" class="Bound">d</a><a id="15641" class="Symbol">)</a> <a id="15643" href="Basics.html#13689" class="Function Operator">≡</a> <a id="15645" class="Symbol">(</a><a id="15646" href="Basics.html#15614" class="Bound">b</a> <a id="15648" href="Basics.html#11580" class="Function Operator">||</a> <a id="15651" href="Basics.html#15616" class="Bound">c</a> <a id="15653" href="Basics.html#11580" class="Function Operator">||</a> <a id="15656" href="Basics.html#15618" class="Bound">d</a><a id="15657" class="Symbol">)</a>
<a id="15659" href="Basics.html#15602" class="Function">||-assoc</a> <a id="15668" href="Basics.html#10904" class="InductiveConstructor">tt</a> <a id="15671" href="Basics.html#15671" class="Bound">c</a> <a id="15673" href="Basics.html#15673" class="Bound">d</a> <a id="15675" class="Symbol">=</a> <a id="15677" href="Basics.html#13662" class="InductiveConstructor">refl</a> <a id="15682" class="Symbol">_</a>
<a id="15684" href="Basics.html#15602" class="Function">||-assoc</a> <a id="15693" href="Basics.html#10907" class="InductiveConstructor">ff</a> <a id="15696" href="Basics.html#15696" class="Bound">c</a> <a id="15698" href="Basics.html#15698" class="Bound">d</a> <a id="15700" class="Symbol">=</a> <a id="15702" href="Basics.html#13662" class="InductiveConstructor">refl</a> <a id="15707" class="Symbol">_</a>


</pre> Booleans are a simple type and we can prove some
 non-trivial things just by case analysis.
<pre class="Agda">
<a id="&amp;&amp;-elim-l"></a><a id="15813" href="Basics.html#15813" class="Function">&amp;&amp;-elim-l</a> <a id="15823" class="Symbol">:</a> <a id="15825" class="Symbol">(</a><a id="15826" href="Basics.html#15826" class="Bound">b</a> <a id="15828" href="Basics.html#15828" class="Bound">c</a> <a id="15830" class="Symbol">:</a> <a id="15832" href="Basics.html#10888" class="Datatype">𝔹</a><a id="15833" class="Symbol">)</a> <a id="15835" class="Symbol">→</a> <a id="15837" class="Symbol">(</a><a id="15838" href="Basics.html#15826" class="Bound">b</a> <a id="15840" href="Basics.html#11516" class="Function Operator">&amp;&amp;</a> <a id="15843" href="Basics.html#15828" class="Bound">c</a><a id="15844" class="Symbol">)</a> <a id="15846" href="Basics.html#13689" class="Function Operator">≡</a> <a id="15848" href="Basics.html#10904" class="InductiveConstructor">tt</a>
          <a id="15861" class="Symbol">→</a> <a id="15863" href="Basics.html#15826" class="Bound">b</a> <a id="15865" href="Basics.html#13689" class="Function Operator">≡</a> <a id="15867" href="Basics.html#10904" class="InductiveConstructor">tt</a>
<a id="15870" href="Basics.html#15813" class="Function">&amp;&amp;-elim-l</a> <a id="15880" href="Basics.html#10904" class="InductiveConstructor">tt</a> <a id="15883" href="Basics.html#15883" class="Bound">c</a> <a id="15885" href="Basics.html#15885" class="Bound">p</a> <a id="15887" class="Symbol">=</a> <a id="15889" href="Basics.html#13662" class="InductiveConstructor">refl</a> <a id="15894" class="Symbol">_</a>

<a id="&amp;&amp;-elim-r"></a><a id="15897" href="Basics.html#15897" class="Function">&amp;&amp;-elim-r</a> <a id="15907" class="Symbol">:</a> <a id="15909" class="Symbol">(</a><a id="15910" href="Basics.html#15910" class="Bound">b</a> <a id="15912" href="Basics.html#15912" class="Bound">c</a> <a id="15914" class="Symbol">:</a> <a id="15916" href="Basics.html#10888" class="Datatype">𝔹</a><a id="15917" class="Symbol">)</a> <a id="15919" class="Symbol">→</a> <a id="15921" class="Symbol">(</a><a id="15922" href="Basics.html#15910" class="Bound">b</a> <a id="15924" href="Basics.html#11516" class="Function Operator">&amp;&amp;</a> <a id="15927" href="Basics.html#15912" class="Bound">c</a><a id="15928" class="Symbol">)</a> <a id="15930" href="Basics.html#13689" class="Function Operator">≡</a> <a id="15932" href="Basics.html#10904" class="InductiveConstructor">tt</a>
          <a id="15945" class="Symbol">→</a> <a id="15947" href="Basics.html#15912" class="Bound">c</a> <a id="15949" href="Basics.html#13689" class="Function Operator">≡</a> <a id="15951" href="Basics.html#10904" class="InductiveConstructor">tt</a>
<a id="15954" href="Basics.html#15897" class="Function">&amp;&amp;-elim-r</a> <a id="15964" href="Basics.html#10904" class="InductiveConstructor">tt</a> <a id="15967" href="Basics.html#10904" class="InductiveConstructor">tt</a> <a id="15970" href="Basics.html#15970" class="Bound">p</a> <a id="15972" class="Symbol">=</a> <a id="15974" href="Basics.html#13662" class="InductiveConstructor">refl</a> <a id="15979" class="Symbol">_</a>

</pre>  Notice, Agda does a lot of the work for us when it comes
  to eliminating cases.
  We could choose not to ask Agda to eliminate cases and instead
  use proof by contradictions.
  That would be more ideologically pure for our purposes but
  would require a bit more machinery and this post already needs
  to introduce a lot to do comparatively little.

  We can prove some non-trivial elimination rules.
<pre class="Agda">
<a id="&amp;&amp;-||-elim"></a><a id="16397" href="Basics.html#16397" class="Function">&amp;&amp;-||-elim</a> <a id="16408" class="Symbol">:</a> <a id="16410" class="Symbol">(</a><a id="16411" href="Basics.html#16411" class="Bound">b</a> <a id="16413" href="Basics.html#16413" class="Bound">c</a> <a id="16415" class="Symbol">:</a> <a id="16417" href="Basics.html#10888" class="Datatype">𝔹</a><a id="16418" class="Symbol">)</a> <a id="16420" class="Symbol">→</a> <a id="16422" class="Symbol">(</a><a id="16423" href="Basics.html#16411" class="Bound">b</a> <a id="16425" href="Basics.html#11516" class="Function Operator">&amp;&amp;</a> <a id="16428" href="Basics.html#16413" class="Bound">c</a><a id="16429" class="Symbol">)</a> <a id="16431" href="Basics.html#13689" class="Function Operator">≡</a> <a id="16433" class="Symbol">(</a><a id="16434" href="Basics.html#16411" class="Bound">b</a> <a id="16436" href="Basics.html#11580" class="Function Operator">||</a> <a id="16439" href="Basics.html#16413" class="Bound">c</a><a id="16440" class="Symbol">)</a>
             <a id="16455" class="Symbol">→</a> <a id="16457" href="Basics.html#16411" class="Bound">b</a> <a id="16459" href="Basics.html#13689" class="Function Operator">≡</a> <a id="16461" href="Basics.html#16413" class="Bound">c</a>
<a id="16463" href="Basics.html#16397" class="Function">&amp;&amp;-||-elim</a> <a id="16474" href="Basics.html#10904" class="InductiveConstructor">tt</a> <a id="16477" href="Basics.html#10904" class="InductiveConstructor">tt</a> <a id="16480" href="Basics.html#16480" class="Bound">p</a> <a id="16482" class="Symbol">=</a> <a id="16484" href="Basics.html#13662" class="InductiveConstructor">refl</a> <a id="16489" class="Symbol">_</a>
<a id="16491" href="Basics.html#16397" class="Function">&amp;&amp;-||-elim</a> <a id="16502" href="Basics.html#10907" class="InductiveConstructor">ff</a> <a id="16505" href="Basics.html#10907" class="InductiveConstructor">ff</a> <a id="16508" href="Basics.html#16508" class="Bound">p</a> <a id="16510" class="Symbol">=</a> <a id="16512" href="Basics.html#13662" class="InductiveConstructor">refl</a> <a id="16517" class="Symbol">_</a>



</pre>  ...and classical results like De Morgan's laws.
<pre class="Agda">

<a id="de-morgan-&amp;&amp;"></a><a id="16582" href="Basics.html#16582" class="Function">de-morgan-&amp;&amp;</a> <a id="16595" class="Symbol">:</a> <a id="16597" class="Symbol">(</a><a id="16598" href="Basics.html#16598" class="Bound">b</a> <a id="16600" href="Basics.html#16600" class="Bound">c</a> <a id="16602" class="Symbol">:</a> <a id="16604" href="Basics.html#10888" class="Datatype">𝔹</a><a id="16605" class="Symbol">)</a> <a id="16607" class="Symbol">→</a> <a id="16609" class="Symbol">(</a><a id="16610" href="Basics.html#11463" class="Function Operator">~</a> <a id="16612" class="Symbol">(</a><a id="16613" href="Basics.html#16598" class="Bound">b</a> <a id="16615" href="Basics.html#11516" class="Function Operator">&amp;&amp;</a> <a id="16618" href="Basics.html#16600" class="Bound">c</a><a id="16619" class="Symbol">))</a> <a id="16622" href="Basics.html#13689" class="Function Operator">≡</a> <a id="16624" class="Symbol">(</a><a id="16625" href="Basics.html#11463" class="Function Operator">~</a> <a id="16627" href="Basics.html#16598" class="Bound">b</a> <a id="16629" href="Basics.html#11580" class="Function Operator">||</a> <a id="16632" href="Basics.html#11463" class="Function Operator">~</a> <a id="16634" href="Basics.html#16600" class="Bound">c</a><a id="16635" class="Symbol">)</a>
<a id="16637" href="Basics.html#16582" class="Function">de-morgan-&amp;&amp;</a> <a id="16650" href="Basics.html#10904" class="InductiveConstructor">tt</a> <a id="16653" href="Basics.html#16653" class="Bound">c</a> <a id="16655" class="Symbol">=</a> <a id="16657" href="Basics.html#13662" class="InductiveConstructor">refl</a> <a id="16662" class="Symbol">_</a>
<a id="16664" href="Basics.html#16582" class="Function">de-morgan-&amp;&amp;</a> <a id="16677" href="Basics.html#10907" class="InductiveConstructor">ff</a> <a id="16680" href="Basics.html#16680" class="Bound">c</a> <a id="16682" class="Symbol">=</a> <a id="16684" href="Basics.html#13662" class="InductiveConstructor">refl</a> <a id="16689" class="Symbol">_</a>

<a id="de-morgan-||"></a><a id="16692" href="Basics.html#16692" class="Function">de-morgan-||</a> <a id="16705" class="Symbol">:</a> <a id="16707" class="Symbol">(</a><a id="16708" href="Basics.html#16708" class="Bound">b</a> <a id="16710" href="Basics.html#16710" class="Bound">c</a> <a id="16712" class="Symbol">:</a> <a id="16714" href="Basics.html#10888" class="Datatype">𝔹</a><a id="16715" class="Symbol">)</a> <a id="16717" class="Symbol">→</a> <a id="16719" class="Symbol">(</a><a id="16720" href="Basics.html#11463" class="Function Operator">~</a> <a id="16722" class="Symbol">(</a><a id="16723" href="Basics.html#16708" class="Bound">b</a> <a id="16725" href="Basics.html#11580" class="Function Operator">||</a> <a id="16728" href="Basics.html#16710" class="Bound">c</a><a id="16729" class="Symbol">))</a> <a id="16732" href="Basics.html#13689" class="Function Operator">≡</a> <a id="16734" class="Symbol">(</a><a id="16735" href="Basics.html#11463" class="Function Operator">~</a> <a id="16737" href="Basics.html#16708" class="Bound">b</a> <a id="16739" href="Basics.html#11516" class="Function Operator">&amp;&amp;</a> <a id="16742" href="Basics.html#11463" class="Function Operator">~</a> <a id="16744" href="Basics.html#16710" class="Bound">c</a><a id="16745" class="Symbol">)</a>
<a id="16747" href="Basics.html#16692" class="Function">de-morgan-||</a> <a id="16760" href="Basics.html#10904" class="InductiveConstructor">tt</a> <a id="16763" href="Basics.html#16763" class="Bound">c</a> <a id="16765" class="Symbol">=</a> <a id="16767" href="Basics.html#13662" class="InductiveConstructor">refl</a> <a id="16772" class="Symbol">_</a>
<a id="16774" href="Basics.html#16692" class="Function">de-morgan-||</a> <a id="16787" href="Basics.html#10907" class="InductiveConstructor">ff</a> <a id="16790" href="Basics.html#16790" class="Bound">c</a> <a id="16792" class="Symbol">=</a> <a id="16794" href="Basics.html#13662" class="InductiveConstructor">refl</a> <a id="16799" class="Symbol">_</a>


</pre>  We can even show nand gates are universal by defining the logical operators
  via nand and showing the new definitions coincide with the old ones. 
<pre class="Agda">
<a id="~&#39;_"></a><a id="16962" href="Basics.html#16962" class="Function Operator">~&#39;_</a> <a id="16966" class="Symbol">:</a> <a id="16968" href="Basics.html#10888" class="Datatype">𝔹</a> <a id="16970" class="Symbol">→</a> <a id="16972" href="Basics.html#10888" class="Datatype">𝔹</a>
<a id="16974" href="Basics.html#16962" class="Function Operator">~&#39;</a> <a id="16977" href="Basics.html#16977" class="Bound">b</a> <a id="16979" class="Symbol">=</a> <a id="16981" href="Basics.html#16977" class="Bound">b</a> <a id="16983" href="Basics.html#11827" class="Function Operator">nand</a> <a id="16988" href="Basics.html#16977" class="Bound">b</a>

<a id="_&amp;&amp;&#39;_"></a><a id="16991" href="Basics.html#16991" class="Function Operator">_&amp;&amp;&#39;_</a> <a id="16997" class="Symbol">:</a> <a id="16999" href="Basics.html#10888" class="Datatype">𝔹</a> <a id="17001" class="Symbol">→</a> <a id="17003" href="Basics.html#10888" class="Datatype">𝔹</a> <a id="17005" class="Symbol">→</a> <a id="17007" href="Basics.html#10888" class="Datatype">𝔹</a>
<a id="17009" href="Basics.html#17009" class="Bound">b</a> <a id="17011" href="Basics.html#16991" class="Function Operator">&amp;&amp;&#39;</a> <a id="17015" href="Basics.html#17015" class="Bound">c</a> <a id="17017" class="Symbol">=</a> <a id="17019" href="Basics.html#16962" class="Function Operator">~&#39;</a> <a id="17022" class="Symbol">(</a><a id="17023" href="Basics.html#17009" class="Bound">b</a> <a id="17025" href="Basics.html#11827" class="Function Operator">nand</a> <a id="17030" href="Basics.html#17015" class="Bound">c</a><a id="17031" class="Symbol">)</a>

<a id="_||&#39;_"></a><a id="17034" href="Basics.html#17034" class="Function Operator">_||&#39;_</a> <a id="17040" class="Symbol">:</a> <a id="17042" href="Basics.html#10888" class="Datatype">𝔹</a> <a id="17044" class="Symbol">→</a> <a id="17046" href="Basics.html#10888" class="Datatype">𝔹</a> <a id="17048" class="Symbol">→</a> <a id="17050" href="Basics.html#10888" class="Datatype">𝔹</a>
<a id="17052" href="Basics.html#17052" class="Bound">b</a> <a id="17054" href="Basics.html#17034" class="Function Operator">||&#39;</a> <a id="17058" href="Basics.html#17058" class="Bound">c</a> <a id="17060" class="Symbol">=</a> <a id="17062" class="Symbol">(</a><a id="17063" href="Basics.html#16962" class="Function Operator">~&#39;</a> <a id="17066" href="Basics.html#17052" class="Bound">b</a><a id="17067" class="Symbol">)</a> <a id="17069" href="Basics.html#11827" class="Function Operator">nand</a> <a id="17074" class="Symbol">(</a><a id="17075" href="Basics.html#16962" class="Function Operator">~&#39;</a> <a id="17078" href="Basics.html#17058" class="Bound">c</a><a id="17079" class="Symbol">)</a>


<a id="~-equiv"></a><a id="17083" href="Basics.html#17083" class="Function">~-equiv</a> <a id="17091" class="Symbol">:</a> <a id="17093" class="Symbol">(</a><a id="17094" href="Basics.html#17094" class="Bound">b</a> <a id="17096" class="Symbol">:</a> <a id="17098" href="Basics.html#10888" class="Datatype">𝔹</a><a id="17099" class="Symbol">)</a> <a id="17101" class="Symbol">→</a> <a id="17103" class="Symbol">(</a><a id="17104" href="Basics.html#16962" class="Function Operator">~&#39;</a> <a id="17107" href="Basics.html#17094" class="Bound">b</a><a id="17108" class="Symbol">)</a> <a id="17110" href="Basics.html#13689" class="Function Operator">≡</a> <a id="17112" class="Symbol">(</a><a id="17113" href="Basics.html#11463" class="Function Operator">~</a> <a id="17115" href="Basics.html#17094" class="Bound">b</a><a id="17116" class="Symbol">)</a>
<a id="17118" href="Basics.html#17083" class="Function">~-equiv</a> <a id="17126" href="Basics.html#10904" class="InductiveConstructor">tt</a> <a id="17129" class="Symbol">=</a> <a id="17131" href="Basics.html#13662" class="InductiveConstructor">refl</a> <a id="17136" class="Symbol">_</a>
<a id="17138" href="Basics.html#17083" class="Function">~-equiv</a> <a id="17146" href="Basics.html#10907" class="InductiveConstructor">ff</a> <a id="17149" class="Symbol">=</a> <a id="17151" href="Basics.html#13662" class="InductiveConstructor">refl</a> <a id="17156" class="Symbol">_</a>

<a id="&amp;&amp;-equiv"></a><a id="17159" href="Basics.html#17159" class="Function">&amp;&amp;-equiv</a> <a id="17168" class="Symbol">:</a> <a id="17170" class="Symbol">(</a><a id="17171" href="Basics.html#17171" class="Bound">b</a> <a id="17173" href="Basics.html#17173" class="Bound">c</a> <a id="17175" class="Symbol">:</a> <a id="17177" href="Basics.html#10888" class="Datatype">𝔹</a><a id="17178" class="Symbol">)</a> <a id="17180" class="Symbol">→</a> <a id="17182" class="Symbol">(</a><a id="17183" href="Basics.html#17171" class="Bound">b</a> <a id="17185" href="Basics.html#16991" class="Function Operator">&amp;&amp;&#39;</a> <a id="17189" href="Basics.html#17173" class="Bound">c</a><a id="17190" class="Symbol">)</a> <a id="17192" href="Basics.html#13689" class="Function Operator">≡</a> <a id="17194" class="Symbol">(</a><a id="17195" href="Basics.html#17171" class="Bound">b</a> <a id="17197" href="Basics.html#11516" class="Function Operator">&amp;&amp;</a> <a id="17200" href="Basics.html#17173" class="Bound">c</a><a id="17201" class="Symbol">)</a>
<a id="17203" href="Basics.html#17159" class="Function">&amp;&amp;-equiv</a> <a id="17212" href="Basics.html#10904" class="InductiveConstructor">tt</a> <a id="17215" href="Basics.html#10904" class="InductiveConstructor">tt</a> <a id="17218" class="Symbol">=</a> <a id="17220" href="Basics.html#13662" class="InductiveConstructor">refl</a> <a id="17225" class="Symbol">_</a>
<a id="17227" href="Basics.html#17159" class="Function">&amp;&amp;-equiv</a> <a id="17236" href="Basics.html#10904" class="InductiveConstructor">tt</a> <a id="17239" href="Basics.html#10907" class="InductiveConstructor">ff</a> <a id="17242" class="Symbol">=</a> <a id="17244" href="Basics.html#13662" class="InductiveConstructor">refl</a> <a id="17249" class="Symbol">_</a>
<a id="17251" href="Basics.html#17159" class="Function">&amp;&amp;-equiv</a> <a id="17260" href="Basics.html#10907" class="InductiveConstructor">ff</a> <a id="17263" href="Basics.html#17263" class="Bound">c</a> <a id="17265" class="Symbol">=</a> <a id="17267" href="Basics.html#13662" class="InductiveConstructor">refl</a> <a id="17272" class="Symbol">_</a>

<a id="||-equiv"></a><a id="17275" href="Basics.html#17275" class="Function">||-equiv</a> <a id="17284" class="Symbol">:</a> <a id="17286" class="Symbol">(</a><a id="17287" href="Basics.html#17287" class="Bound">b</a> <a id="17289" href="Basics.html#17289" class="Bound">c</a> <a id="17291" class="Symbol">:</a> <a id="17293" href="Basics.html#10888" class="Datatype">𝔹</a><a id="17294" class="Symbol">)</a> <a id="17296" class="Symbol">→</a> <a id="17298" class="Symbol">(</a><a id="17299" href="Basics.html#17287" class="Bound">b</a> <a id="17301" href="Basics.html#17034" class="Function Operator">||&#39;</a> <a id="17305" href="Basics.html#17289" class="Bound">c</a><a id="17306" class="Symbol">)</a> <a id="17308" href="Basics.html#13689" class="Function Operator">≡</a> <a id="17310" class="Symbol">(</a><a id="17311" href="Basics.html#17287" class="Bound">b</a> <a id="17313" href="Basics.html#11580" class="Function Operator">||</a> <a id="17316" href="Basics.html#17289" class="Bound">c</a><a id="17317" class="Symbol">)</a>
<a id="17319" href="Basics.html#17275" class="Function">||-equiv</a> <a id="17328" href="Basics.html#10904" class="InductiveConstructor">tt</a> <a id="17331" href="Basics.html#17331" class="Bound">c</a> <a id="17333" class="Symbol">=</a> <a id="17335" href="Basics.html#13662" class="InductiveConstructor">refl</a> <a id="17340" class="Symbol">_</a>
<a id="17342" href="Basics.html#17275" class="Function">||-equiv</a> <a id="17351" href="Basics.html#10907" class="InductiveConstructor">ff</a> <a id="17354" href="Basics.html#10904" class="InductiveConstructor">tt</a> <a id="17357" class="Symbol">=</a> <a id="17359" href="Basics.html#13662" class="InductiveConstructor">refl</a> <a id="17364" class="Symbol">_</a>
<a id="17366" href="Basics.html#17275" class="Function">||-equiv</a> <a id="17375" href="Basics.html#10907" class="InductiveConstructor">ff</a> <a id="17378" href="Basics.html#10907" class="InductiveConstructor">ff</a> <a id="17381" class="Symbol">=</a> <a id="17383" href="Basics.html#13662" class="InductiveConstructor">refl</a> <a id="17388" class="Symbol">_</a>


</pre>  Later, we would like to formally show negative results that `_&&_` and
  `_||_` are not universal. 
  
  Thank you for reading.
  Any questions or (polite) criticisms are appreciated.

  Next post should: we introduce the natural numbers `ℕ`, equational
  reasoning, and UF-style proofs by contradiction. 

  There is a
  [public repository](https://gitlab.com/maxbaroi/agda-posts)
  of the literate Agda files used to develop these
  posts.

  This post was created from a literate Agda file using the method described by
  [Jesper Cockx](https://jesper.sikanda.be/posts/literate-agda.html). 
