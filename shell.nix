let
  pkgs = import <nixpkgs> {};

in
pkgs.mkShell {
  buildInputs = [
    pkgs.haskellPackages.Agda
    pkgs.AgdaStdlib
    pkgs.pandoc
  ];
}
